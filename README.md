HEIA project report LaTeX template
==================================

A LaTeX template following the HEIA recommendations for project reports.

Author: Marc Demierre <marc.demierre@gmail.com>

Features
--------

* Nice title page with school logo, project info and project logo
* Code highlighting
* Smart cross-references support (using autoref)
* "Abstract" page
* "Authors" page
* Table of contents
* Header and footer with project information
* Glossary (acronyms and terms)
* Bibliography


Dependencies
------------

* A LaTeX distribution
* biber (for bibliography)
* Python 2.7+ or 3+ and pygments (for the minted code highlighting package)
* Perl 5 (a recent version) (for the xindy indexing package)

If you encounter issues, please make sure everything is in your `PATH`.

### Setup on OSX

* Install a LaTeX distribution, like MacTex (https://tug.org/mactex/). Make sure the bin directory is in your `PATH`.

* Python is already installed, you just need the pygments package:

        pip install pygments
    
* Perl is already installed, nothing to do.

You can also use Homebrew to install more recent versions of Python and Perl.

### Setup on Windows

* Install a LaTeX distribution, like MiKTeX (http://miktex.org/). Make sure the bin directory is in your `PATH`.
* Install the biber package. See http://tex.stackexchange.com/questions/154708/how-can-one-install-biber-on-miktex-64-bit to know how.
* Install Python from https://www.python.org/downloads/windows/. Be sure to check the PATH option in the installer.
* Install the pygments package
        
        pip install pygments
        
* Install a Perl distribution from https://www.perl.org/get.html#win32 (recommended: ActivePerl)

### Setup on Linux

TODO

Getting started
---------------

* Install the dependencies
* Fill your project details in `config/metadata.tex`

### Using the command line

* Build using the following commands:

        pdflatex -shell-escape main
        makeglossaries main
        biber main
        pdflatex -shell-escape main
        pdflatex -shell-escape main
        
### Using the Texmaker IDE

* Use this configuration for the quick build:

        pdflatex -shell-escape -synctex=1 -interaction=nonstopmode %.tex|makeglossaries %|biber %|pdflatex -shell-escape -synctex=1 -interaction=nonstopmode %.tex|pdflatex -shell-escape -synctex=1 -interaction=nonstopmode %.tex


Contribute
----------

Do not hesitate to make a pull request if you have useful additions/corrections for this template. You can also post an issue if you find a bug or want to suggest an improvement.

The following contributions would be greatly appreciated:

* Dependencies setup guide for Windows & Linux, and precisions for OSX
* Makefile to simplify the build process from the command line
* Internationalization (especially French support)


Special thanks
--------------

I'd like to thank:

* Sylvain Julmy, for sharing his template (which I used as a base for my title page) and his precious advice on LaTeX
* Damien Raemy, for his tests on the Windows platform

License
-------

    Copyright 2015 Marc Demierre

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.